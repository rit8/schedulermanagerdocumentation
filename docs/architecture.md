# Scheduler Manager Architecture

## System Architecture

The Scheduler Manager is a solution for schedule jobs, management and analysis of results, to support your business and marketing strategies.
To this goal, the Scheduler Manager follow the architecture that is possible to see in next image.

!["Architecture"](./img/architecture.png) *Fig.1: Scheduler Manager architecture*

 
##  Database Model

The Scheduler Manager system must have the capability to store job configurations and report history, for this the system have the following Database: 

 
|Scheduler Manager Database|
| :---: |
|!["DB"](./img/database.png)| 

In database model, the table *BLEntityVersionConfiguration* permits the installation of version configurations in the system. These versions
define the system behavior in each job operation. Configurations like timeExpression to define the time of execution of the job.

This database model also have history record to store the execution information of each job, if the job completed successfully, time of the execution, etc.
The table *ReConfigScheduledJob* store the configuration of each job, if permit parallel execution, the minutes expected of execution.

