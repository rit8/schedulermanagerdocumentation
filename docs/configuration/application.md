# Scheduler Manager Configuration



| Name | Description | Type | Default value
-----| ----------- | ---- | ------------- |
logging.level.ROOT| Define the logging level for logs from ROOT | string | INFO |
logging.level.io.github.jhipster| Define the logging level for logs from jHipster | string  | INFO |
logging.level.com.readinessit.schedulermanager| Define the logging level for logs for the schedulermanager | string  | INFO |
app.scheduling.report-url | Report URL for Scheduler Manager | String | http://{{ip}}:{{port}}/services/schedulermanager/api/schedule/reporting
app.script.ssh.timeout | Timeout value for ssh connections | Long | 5000
app.script.ssh.sleep | Sleep value for ssh connections | Long | 1000
spring.datasource.url | Define the url connection to the database | string |jdbc:postgresql://localhost:5432/schedulermanager |
spring.datasource.username | Define the username to connection in database | string | postgres |
spring.datasource.password | Define the password to connection in database | string | admin |
app.api.unique-entry| Enable only unique entry for business configurations| boolean | false|
app.api.pageable.page-size| Define default page size for pages returned from database| integer | 20|
app.services.use-cache| Enable use of cache in the system| boolean | true|
app.services.name| Define the name to register in the master | string| scheduler-manager |
app.services.schedule-call| Default class to call the jobs | string| com.readinessit.schedulermanager.service.scheduler.schedules.ScheduleCall |
app.services.pool-size|the size of a thread pool that runs the schedulers | long| 10 |
app.services.voter.fixed-delay|Define the time to update the vote in the master, this time needs to be less that no-active-heartbeat-seconds config | long| 60 |
app.services.active.no-active-heartbeat-seconds|The time passed for the scheduler to consider that the master has died | long| 90 |
