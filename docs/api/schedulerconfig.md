# SchedulerManager - Schedules Bussiness Logic API
This is an API for SchedulerManager - Business Logic API powered by ReadinessIT

## Version: 1.0.0

**License:** [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

### /deployment/business-logic/entity-type/{entityType}

#### POST
##### Summary

createEntityVersion

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityType | path | The type of the Entity | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Created |
| 400 | Bad Request |

### /deployment/business-logic/entity-type/{entityType}/active

#### GET
##### Summary

getActiveEntityByType

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityType | path | The type of the Entity | Yes | string |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get active Entity by Type |

### /deployment/business-logic/entity-type/{entityType}/entity-name/{entityName}/active

#### GET
##### Summary

getActiveEntityByTypeAndName

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityType | path | The type of the Entity | Yes | string |
| entityName | path | The name of the Entity | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get active Entity by Type |
| 404 | Not found |

### /deployment/business-logic/entity-type/{entityType}/entity-name/{entityName}

#### DELETE
##### Summary

deleteEntityByName

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityType | path | The type of the Entity | Yes | string |
| entityName | path | The name of the Entity | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Version Deleted |
| 404 | Not found |

### /deployment/business-logic/entity-type/{entityType}/entity-name/{entityName}/version/{version}

#### DELETE
##### Summary

deleteEntityByVersion

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| entityType | path | The type of the Entity | Yes | string |
| entityName | path | The name of the Entity | Yes | string |
| version | path | The version | Yes | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Version Deleted |
| 404 | Not found |

### /deployment/business-logic/refresh

#### GET
##### Summary

refreshAllConfigurations

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | all Configurations Uploaded |

### /schedulers/config

#### POST
##### Summary

createSchedulerConfig

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| 400 | Bad Request |

#### GET
##### Summary

listOfSchedulesConfig

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of Schedules Configuration |
| 400 | Bad Request |

### /schedulers/config/{version}

#### GET
##### Summary

getSchedulerConfigByVersion

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| version | path | Version of the configuaration | Yes | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Get of Scheduler Config By Version |
| 404 | Not Found |

#### DELETE
##### Summary

deleteSchedulerVersionConfiguration

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| version | path | delete the Scheduler Version Configuration | Yes | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Delete of the Version |
| 404 | Not Found |

### /schedulers/config/active

#### GET
##### Summary

getActiveSchedulerConfiguration

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Get Active Scheduler Configuration |
| 404 | Not Found |

### /schedulers/config/deprecated

#### GET
##### Summary

listDeprecatedSchedulers

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Get list Deprecated Scheduler Configuration |

### /schedulers/config/{version}/activation-manager

#### PUT
##### Summary

businessLogicActivationManager

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| version | path | The version | Yes | integer |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Success |
| 400 | Bad Request |

### /schedulers/{name}/action/start

#### PUT
##### Summary

StartOfSchedule

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | Schedule Name | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Success |
| 400 | Bad Request |
| 404 | Not Found |

### /schedulers/{name}/action/stop

#### PUT
##### Summary

StopOfSchedule

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | Schedule Name | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Success |
| 400 | Bad Request |
| 404 | Not Found |

### /schedulers/{name}/action/re-schedule

#### PUT
##### Summary

ReScheduleOfSchedule

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | Schedule Name | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Success |
| 400 | Bad Request |
| 404 | Not Found |

### /configuration/config-scheduler

#### POST
##### Summary

createConfigScheduler

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| 400 | Bad Request |

#### GET
##### Summary

listConfigScheduler

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of Config Scheduler |

### /configuration/config-scheduler/application-name/{appName}

#### GET
##### Summary

listConfigSchedulerByAppName

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| appName | path | The appName of the Config Scheduler | Yes | string |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of Config Scheduler |

### /configuration/config-scheduler/application-name/{appName}/alias/{alias}

#### PUT
##### Summary

updateConfigScheduler

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| appName | path | The appName of the Config Scheduler | Yes | string |
| alias | path | The alias of the Config Scheduler | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Success |
| 400 | Bad Request |

#### DELETE
##### Summary

deleteConfigScheduler

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| appName | path | The appName of the Config Scheduler | Yes | string |
| alias | path | The alias of the Config Scheduler | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Delete of the scheduler config |
| 404 | Not Found |

#### GET
##### Summary

getConfigSchedulerByAppNameAndAlias

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| appName | path | The appName of the Config Scheduler | Yes | string |
| alias | path | The alias of the Config Scheduler | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of Config Scheduler |
| 404 | Not Found |

### /configuration/job-type

#### POST
##### Summary

createJobType

##### Responses

| Code | Description |
| ---- | ----------- |
| 201 | Success |
| 400 | Bad Request |

#### GET
##### Summary

listJobType

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of Job Type |

### /configuration/job-type/name/{name}

#### PUT
##### Summary

updateJobType

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | The name of the resource | Yes | string (int64) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Success |
| 400 | Bad Request |

#### DELETE
##### Summary

deleteJobType

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | The name of the resource | Yes | string (int64) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | Delete of the Job Type |
| 404 | Not Found |

#### GET
##### Summary

getJobTypeByName

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | The name of the resource | Yes | string (int64) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of Job Type |
| 404 | Not Found |

### /configuration/scheduler/active

#### GET
##### Summary

listScheduler

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of Scheduler |

### /configuration/scheduler/{name}/active

#### GET
##### Summary

getScheduler

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | The name of the resource | Yes | string (int64) |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of Scheduler |
| 404 | Not Found |

### /configuration/schedulers

#### GET
##### Summary

listAllSchedulers

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of Schedulers |

### /configuration/schedulers/{name}

#### GET
##### Summary

getAllSchedulersByName

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | path | The name of the resource | Yes | string (int64) |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of All Schedulers By Name |

### /configuration/error-end-causes

#### GET
##### Summary

listAllErrorEndCauses

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of end Causes |

### /configuration/error-end-causes/error-end-cause-id/{errorEndCauseId}

#### GET
##### Summary

getErrorEndCause

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| errorEndCauseId | path | The error end cause id of the resource | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get Error End Cause |
| 404 | Not Found |

### /configuration/error-system-logic-map

#### GET
##### Summary

listAllErrorSystemLogicMap

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | list of Error System Logic Map |

### /configuration/error-system-logic-map/error-system-logic-map-id/{errorSystemLogicMapId}

#### GET
##### Summary

getErrorSystemLogicMap

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| errorSystemLogicMapId | path | The error system logic map id of the resource | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get Error System Logic Map |
| 404 | Not Found |

### Models

#### BLErrorEndCause

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| errorEndCauseId | string | Error End Cause Identifier<br>_Example:_ `123456` | No |
| name | string | Error End Cause Name<br>_Example:_ `"Code Name 1"` | No |
| value | string | Error End Cause Value<br>_Example:_ `"Date_Expired"` | No |
| description | string | Error End Cause Description<br>_Example:_ `"The expiration date has passed"` | No |
| errorSystemLogicMapId | integer | Error System Logic Map Identifier<br>_Example:_ `2` | No |

#### ErrorSystemLogicMapAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| errorSystemLogicMapId | string | Error System Logic Map Identifier<br>_Example:_ `12345` | No |
| endCause | string | EndCause Name<br>_Example:_ `"Success"` | Yes |
| endCauseDescription | string | EndCause Description<br>_Example:_ `"SuccessDescription"` | No |

#### PageableResult

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| PageableResult | array |  |  |

#### SingleScheduler

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| version | long | Entity Version | No |
| entityType | string | Entity Type Name<br>_Example:_ `"EntityExample"` | No |
| activationDate | dateTime | Entity Activation Date | No |
| schemaVersion | long | Entity Schema Version | No |
| entityName | string | Entity Name<br>_Example:_ `"EntityNameExample"` | No |
| user | string | Entity User<br>_Example:_ `"UserExample"` | No |
| description | string | Entity Description<br>_Example:_ `"DescriptionExample"` | No |
| active | boolean | Entity Active | No |
| scheduler | object |  | No |

#### EntityConfigurationAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| version | integer | Entity Version<br>_Example:_ `4` | Yes |
| entityType | string | Entity Type Name<br>_Example:_ `"EntityNameExample"` | Yes |
| activationDate | dateTime | Entity Activation Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | No |
| schemaVersion | integer | Entity Schema Version<br>_Example:_ `3` | Yes |
| entityName | string | Entity Name<br>_Example:_ `"NameExample"` | Yes |
| user | string | Entity User<br>_Example:_ `"UserExample"` | No |
| description | string | Entity Description<br>_Example:_ `"DescriptionExample"` | No |
| active | boolean | Entity Active<br>_Example:_ `true` | Yes |
| payload | string | Entity Payload Definition<br>_Example:_ `"Definition of entity"` | Yes |

#### SchedulersVersionConfigurationAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| version | integer | Configuration Version<br>_Example:_ `1` | Yes |
| activationDate | dateTime | Configuration Activation Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | Yes |
| user | string | Configuration User<br>_Example:_ `"John Doe"` | Yes |
| name | string | Configuration Name<br>_Example:_ `"version1.0.0"` | Yes |
| description | string | Configuration Description<br>_Example:_ `"Complete configuration of the tables"` | No |
| active | boolean | Configuration Active<br>_Example:_ `true` | Yes |
| blSchedulers | [ object ] |  | No |

#### BlScheduler

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| name | string | Scheduler Name<br>_Example:_ `"Move To History"` | Yes |
| description | string | Scheduler Description<br>_Example:_ `"Schedule to move used vouchers to History"` | No |
| alias | string | Scheduler Alias<br>_Example:_ `"MoveToHistoryScheduler"` | Yes |
| applicationName | string | Application Name<br>_Example:_ `"vouchers"` | Yes |
| timeExpressionMode | string | Scheduler Time Expression Mode<br>_Example:_ `"CRON"` | Yes |
| timeExpression | string | Scheduler Time Expression<br>_Example:_ `"* * * * *"` | Yes |
| state | string | Scheduler State<br>_Example:_ `"STARTED"` | Yes |
| jobType | string | Scheduler Type<br>_Example:_ `"Housekeeping"` | No |
| entryParameters | [ object ] | Scheduler Entry Parameters | No |

#### entryParametersOptional

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| key | string | Entry Parameter Key Name<br>_Example:_ `"query"` | No |
| type | string | Entry Parameter Value | No |
| optional | boolean | Difine if key is optional | No |

#### exitParametersOptional

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| key | string | Entry Parameter Key Name<br>_Example:_ `"query"` | No |
| type | string | Entry Parameter Value | No |
| optional | boolean | Difine if key is optional | No |

#### EntryParameters

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| key | string | Entry Parameter Key Name | No |
| value | string | Entry Parameter Value | No |

#### ErrorAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| code | string | Error Code<br>_Example:_ `"Code 010"` | Yes |
| reason | string | Error Reason<br>_Example:_ `"the reason"` | Yes |
| message | string | Error Message<br>_Example:_ `"the message"` | No |
| reference | string | Error Reference<br>_Example:_ `"https://www.scheduler-manager.com/problem/bad-request"` | No |

#### ActivationManager

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| version | integer | Version<br>_Example:_ `1` | Yes |
| active | boolean | Enable Active | No |
| activationDate | dateTime | Version Activation Date | No |

#### ScheduleManager

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| name | string | Scheduler Name | Yes |
| timeExpressionMode | string | Scheduler Time Expression Mode | Yes |
| timeExpression | string | Scheduler Time Expression | Yes |

#### ConfigScheduler

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| alias | string | Config Scheduler Alias<br>_Example:_ `"move-expired-to-history"` | Yes |
| applicationName | string | Application Name<br>_Example:_ `"vouchers"` | Yes |
| method | string | Config Scheduler Method<br>_Example:_ `"http://localhost:8484/services/vouchermanager/api/jobs/vouchers/move-to-history/expired"` | Yes |
| description | string | Config Scheduler Description<br>_Example:_ `"This schedule will move expired vouchers to history"` | No |
| entryParameters | [ object ] |  | No |
| exitParameters | [ object ] |  | No |
| jobType | string | Job type of the schedulerConfig<br>_Example:_ `"Business"` | Yes |
| permitParallelExecution | boolean | Permit parallel Execution of jobs | Yes |
| minutesExpectedOfExecution | long | Minutes Expected of Execution of the job<br>_Example:_ `30` | No |

#### JobType

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| name | string | Job Type name<br>_Example:_ `"business"` | Yes |
| description | string | Description of job Type<br>_Example:_ `"Job type for business"` | Yes |

#### ReportScheduler

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| scheduleName | string | Report Scheduler Name<br>_Example:_ `"move-expired-to-history"` | No |
| applicationName | string | Application Name<br>_Example:_ `"vouchers"` | No |
| jobStartRunDate | string | Report Start Run Date<br>_Example:_ `"http://localhost:8484/services/vouchermanager/api/jobs/vouchers/move-to-history/expired"` | No |
| jobEndRunDate | string | Report End Run Date<br>_Example:_ `"http://localhost:8484/services/vouchermanager/api/jobs/vouchers/move-to-history/expired"` | No |
| runState | string | Report Run State<br>_Example:_ `"[{'key':'Entry_Date','type':'DATE','optional':'true'}]"` | No |
| description | string | Report Run State<br>_Example:_ `"This schedule will move expired vouchers to history"` | No |
| exitParameters | string | _Example:_ `"exit parameters"` | No |
