# Scheduler Manager - Scheduler Management API
This is an API for SchedulerManager - Scheduler Management API powered by ReadinessIT

## Version: 1.1.0

**License:** [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

### /history/scheduler-reports

#### GET
##### Summary

listSchedulerReport

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| scheduleName | query | The schedule name | No | string |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of Scheduler Report |

### /history/scheduler-reports/scheduler-report-id/{schedulerReportId}

#### GET
##### Summary

getSchedulerReport

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| schedulerReportId | path | The scheduler report id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of Scheduler Report |
| 404 | Not Found |

### /scheduler-controller

#### GET
##### Summary

listSchedulerController

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| name | query | The name of the resource | No | string |
| alias | query | The alias of the Config Scheduler | No | string |
| page | query | Page Number | No | integer |
| size | query | Number of items to return at one time | No | integer |
| sort | query | Sorting criteria in the format property(,asc\\desc). Default sort order is ascending.Multiple sort criteria are supported. | No | [ string ] |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of Scheduler Controller |

### /scheduler-controller/scheduler-controller-id/{schedulerControllerId}

#### GET
##### Summary

getSchedulerController

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| schedulerControllerId | path | The schedulerController id | Yes | string |

##### Responses

| Code | Description |
| ---- | ----------- |
| 200 | get of Scheduler Controller |
| 404 | Not Found |

### Models

#### PageableResult

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| PageableResult | array |  |  |

#### SchedulerReportAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| schedulerReportId | string | Scheduler Report Identifier<br>_Example:_ `13` | No |
| scheduleName | string | Scheduler Name<br>_Example:_ `"moveHistory"` | No |
| applicationName | string | Application Name<br>_Example:_ `"vouchers"` | No |
| jobStartRunDate | dateTime | Job Start Run Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | No |
| jobEndRunDate | dateTime | Job End Run Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | No |
| runState | string | Run State<br>_Example:_ `0` | No |
| description | string | Scheduler Report Description<br>_Example:_ `"Description Example"` | No |
| exitParameters | string | Scheduler Report Exit Parameters<br>_Example:_ `"exit Parameters Example"` | No |

#### SchedulerControllerAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| schedulerControllerId | string | Scheduler Controller Identifier<br>_Example:_ `123` | No |
| name | string | Scheduler Controller Name<br>_Example:_ `"moveHistory"` | No |
| applicationName | string | Application Name<br>_Example:_ `"vouchers"` | No |
| description | string | Scheduler Controller Description<br>_Example:_ `"description"` | No |
| alias | string | Scheduler Controller Alias<br>_Example:_ `"alias Example"` | No |
| timeExpression | string | Scheduler Controller Time Expression<br>_Example:_ `"* * * * * *"` | No |
| timeExpressionMode | string | Scheduler Controller Expression Mode<br>_Example:_ `"CRON"` | No |
| state | string | Scheduler Controller Styate Name<br>_Example:_ `"start"` | No |
| actionRequest | string | Scheduler Controller Action Request<br>_Example:_ `"action example"` | No |
| jobLastStartRunDate | dateTime | Scheduler Last Start Run Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | No |
| jobLastSuccessfulRunDate | dateTime | Scheduler Controller Last Successful Run Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | No |
| jobLastRunDate | dateTime | Scheduler Controller Last Run Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | No |
| jobNextRunDate | dateTime | Job End Run Date<br>_Example:_ `"2018-08-29T09:12:33.001Z"` | No |

#### ErrorAPI

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| code | string | Error Code<br>_Example:_ `8` | Yes |
| reason | string | Error Reason<br>_Example:_ `"the reason"` | Yes |
| message | string | Error Message<br>_Example:_ `"the message"` | No |
| reference | string | Error Reference<br>_Example:_ `"https://www.scheduler-manager.com/problem/bad-request"` | No |
