# Scheduler Manager


##Main Features

* Control of schedulers executions across different instances

* Multiple ways of scheduling the scheduler (Now implement: cron, fixed_delay)

* Report of the schedulers executions (execution times, execution Result, etc)

* Possibility of make action on already working schedulers (start, stop)

* Don´t run multiple executions on non possible parallel schedulers

* Executors
    * HTTP Executor
    * Shell executor

* Metrics
    * Prometheus
    * ElasticSearch

* Cloud Native
    * Runs on cloud environment like kubernetes/docker
