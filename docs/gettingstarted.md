# Getting Started

## Step 1 - Config Scheduler Job Definition

The initial step is to configure the scheduler job and define what the job does, the following URL and JSON show 
the configuration for the job *run-workflowengine-housekeeping*:

> POST http://{{ip}}:{{port}}/services/schedulermanager/api/v1/configuration/config-scheduler

```json
{
    "alias": "run-workflowengine-housekeeping",
    "applicationName": "workflowengine",
    "method": "http://localhost:8080/services/workflowengine/api/v1/scheduler/housekeeping",
    "description": "Execute workflow engine housekeeping",
    "entryParameters": [
        {
            "key": "query",
            "type": "STRING",
            "optional": "false"
        }
    ],
    "exitParameters": null,
    "jobType": "Business",
    "permitParallelExecution": false,
    "minutesExpectedOfExecution": 10
}
```


Lets understanding each field of the json:

| Parameter Name                            | Description                               |
| :---------------------------------------- | :---------------------------------------- |
| `Alias`                                   | The name of job |
| `ApplicationName`                         | Name of the destination system |
| `Method`                                  | Method to use, in the example is an HTTP request to external system |
| `Description`                             | A short description of what the job do |
| `EntryParameters`                         | Specification of the entryParameters to send to the external system, in the example is specified one parameter query of type string that is not optional |
| `JobType`                                 | Specify the type of job |
| `PermitParallelExecution`                 | Permit executions of the job in parallel, by default is false |
| `MinutesExpectedOfExecution`              | Specify the expected execution of time in minutes of the job takes to run |



## Step 2 - Scheduler Version Configuration Definition

To configure the behavior of each job, we need to create a version configuration on the system, the following JSON example
shows a configuration for the job *workflowengine-housekeeping-workflow-execution*:

> POST http://{{ip}}:{{port}}/services/schedulermanager/api/v1/deployment/business-logic/entity-type/Scheduler

```json
{
    "version": 1,
    "entityType": "Scheduler",
    "activationDate": "2018-08-29T09:12:33.001Z",
    "schemaVersion": 1,
    "entityName": "workflowengine-housekeeping-workflow-execution",
    "user": "RIT",
    "description": "Schedule to make houseekeping for the TABLE WORKFLOW_EXECUTION, clean the last 7 days",
    "active": true,
    "payload": "{\r\n    \"name\": \"workflowengine-housekeeping-workflow-execution\",\r\n    \"description\": \"Schedule to make houseekeping for the TABLE WORKFLOW_EXECUTION, clean the last 7 days\",\r\n    \"alias\": \"run-workflowengine-housekeeping\",\r\n    \"applicationName\": \"workflowengine\",\r\n    \"timeExpressionMode\": \"CRON\",\r\n    \"timeExpression\": \"0 0 0 * * *\",\r\n    \"state\": \"STARTED\",\r\n    \"entryParameters\": [\r\n        {\r\n            \"key\": \"query\",\r\n            \"value\": \"declare CURSOR PartTables IS SELECT TABLE_NAME, PARTITION_NAME, HIGH_VALUE FROM USER_TAB_PARTITIONS WHERE TABLE_NAME = 'WORKFLOW_EXECUTION';  highValue TIMESTAMP;  BEGIN FOR aTab IN PartTables LOOP EXECUTE IMMEDIATE 'BEGIN :ret := '||aTab.HIGH_VALUE||'; END;' USING OUT highValue; IF highValue < sysdate-7 THEN DBMS_OUTPUT.PUT_LINE(highValue); EXECUTE IMMEDIATE 'ALTER TABLE WORKFLOW_EXECUTION DROP PARTITION '||aTab.PARTITION_NAME||' UPDATE INDEXES'; END IF; END LOOP; END;\"\r\n        }\r\n    ],\r\n    \"jobType\": \"Business\"\r\n}"
}
```

Lets understanding each field of the json:

| Parameter Name                            | Description                               |
| :---------------------------------------- | :---------------------------------------- |
| `Version`                                 | Version of each configuration |
| `EntityType`                              | Entity type of the version configuration |
| `ActivationDate`                          | Date for the activation of the version configuration, it can be set to the future  |
| `SchemaVersion`                           | Schema version of version configuration |
| `EntityName`                              | Entity name of version configuration, in this example the name of the job |
| `User`                                    | Name of the user that is creating the version configuration |
| `Description`                             | Description of the job |
| `Active`                                  | Boolean to specify if the version is active or not |
| `Payload`                                 | Configuration of the job, configuration of time scheduler, parameters, etc. |



In the payload we but the configuration for the job, specify the time of execution, state of the job, entryParameters,
lets understanding this payload:

```json
{
    "name": "workflowengine-housekeeping-workflow-execution",
    "description": "Schedule to make houseekeping for the TABLE WORKFLOW_EXECUTION, clean the last 7 days",
    "alias": "run-workflowengine-housekeeping",
    "applicationName": "workflowengine",
    "timeExpressionMode": "CRON",
    "timeExpression": "0 0 0 * * *",
    "state": "STARTED",
    "entryParameters": [
        {
           "key": "query",         
           "value": "declare CURSOR PartTables IS SELECT TABLE_NAME, PARTITION_NAME, HIGH_VALUE FROM USER_TAB_PARTITIONS WHERE TABLE_NAME = 'WORKFLOW_EXECUTION';  highValue TIMESTAMP;  BEGIN FOR aTab IN PartTables LOOP EXECUTE IMMEDIATE 'BEGIN :ret := '||aTab.HIGH_VALUE||'; END;' USING OUT highValue; IF highValue < sysdate-7 THEN DBMS_OUTPUT.PUT_LINE(highValue); EXECUTE IMMEDIATE 'ALTER TABLE WORKFLOW_EXECUTION DROP PARTITION '||aTab.PARTITION_NAME||' UPDATE INDEXES'; END IF; END LOOP; END;"
        }
    ],
    "jobType": "Business"
}
```

| Parameter Name                            | Description                               |
| :---------------------------------------- | :---------------------------------------- |
| `Name`                                    | The name of job |
| `Description`                             | A short description of what the job do |
| `Alias`                                   | The name of the job that link to the configuration scheduler job table that we configure in the first step |
| `ApplicationName`                         | Name of the destination system |
| `TimeExpressionMode`                      | Expression of the time mode to use in the example *CRON* |
| `TimeExpression`                          | Configuration of the CRON |
| `State`                                   | The initial state of the job, in this case will have a state of *STARTED* |
| `EntryParameters`                         | The entryParameters that the job need to execute, in this case the end system needs the query to clean the partition |
| `JobType`                                 | Specify the type of job |


After the version configuration activates the scheduler manager will start every job. 
The scheduler manager has a master job that runs every 1 min to verify for new version configurations and to analyze 
if new actions are made to the job already running, for example, start and stop actions. 


## Step 3 - Commons Scheduler API Integration

In each external system that implements the logic of the job is necessary to use the API of reportCall to create a report of job status in the Scheduler Manager, to do this
is necessary to import the commons-scheduler library in the project. 

- [X] Bellow you can see the pom import for maven repository:

```xml
<dependency>
    <groupId>com.readinessit.commons</groupId>
    <artifactId>commons-scheduler</artifactId>
    <version>${commons-scheduler.version}</version>
</dependency>
```
<br>

- [X] Important configuration for the library, in the application properties of the project is necessary to configure the endpoint for the schedulerManager service:

```yaml
commons:
    scheduler:
        scheduler-server:
            report-url: http://{{ip}}:{{port}}/services/schedulermanager/api/schedule/reporting
```

<br>
    
- [X] To create a report for the job execution is required to use the following method:

```java
void reportCall(Boolean success, Instant startTime, String runState, String descriptionState, Map<String, Object> exitParameters, String transactionId)
```

<br>

Example of use:

```java

    @Autowired
    private SchedulerReportService schedulerReportService;

    schedulerReportService.reportCall(Boolean.TRUE, startTime, WorkflowStatus.COMPLETED.name(),  "Success", new HashMap<>(),  schedulerApiDto.getTransactionId());
```

## Step 4 - Management of Schedulers

When the job is already running is possible to manage the state of the job, it's possible to stop, start and re-scheduler the job.

Note: When a request of start or stop is made it is important to know that action will not be executed at the moment. 
The scheduler manager has a master job that runs every 1 min after this job runs will detect the changes and apply the modification for the job.
   
- start    
> PUT http://{{ip}}:{{port}}/services/schedulermanager/api/v1/schedulers/workflowengine-housekeeping-workflow-execution/action/start

- stop
> PUT http://{{ip}}:{{port}}/services/schedulermanager/api/v1/schedulers/workflowengine-housekeeping-workflow-execution/action/stop

- re-schedule
> PUT http://{{ip}}:{{port}}/services/schedulermanager/api/v1/schedulers/workflowengine-housekeeping-workflow-execution/action/re-schedule
```json
{
    "name": "workflowengine-housekeeping-workflow-execution",
    "timeExpressionMode": "DELAY",
    "timeExpression": "30000"
}
```


!!! Notes

When creating a versionConfiguration its possible to use an alternative interface that is more user friendly you can see bellow the URL and the json example:

> POST http://{{ip}}:{{port}}/services/schedulermanager/api/v1/schedulers/config

```json
    {
        "version": 1,
        "activationDate": "2018-08-29T09:12:33.001Z",
        "user": "RIT",
        "name": "workflowengine-housekeeping-workflow-execution",
        "description": "Schedule to make houseekeping for the TABLE WORKFLOW_EXECUTION, clean the last 7 days",
        "active": true,
        "blSchedulers": [
            {
                "name": "workflowengine-housekeeping-workflow-execution",
                "description": "Schedule to make houseekeping for the TABLE WORKFLOW_EXECUTION, clean the last 7 days",
                "alias": "run-workflowengine-housekeeping",
                "applicationName": "workflowengine",
                "timeExpressionMode": "CRON",
                "timeExpression": "0 0 0 * * *",
                "state": "STARTED",
                "entryParameters": [
                    {
                        "key": "query",
                        "value": "declare CURSOR PartTables IS SELECT TABLE_NAME, PARTITION_NAME, HIGH_VALUE FROM USER_TAB_PARTITIONS WHERE TABLE_NAME = 'WORKFLOW_EXECUTION';  highValue TIMESTAMP;  BEGIN FOR aTab IN PartTables LOOP EXECUTE IMMEDIATE 'BEGIN :ret := '||aTab.HIGH_VALUE||'; END;' USING OUT highValue; IF highValue < sysdate-7 THEN DBMS_OUTPUT.PUT_LINE(highValue); EXECUTE IMMEDIATE 'ALTER TABLE WORKFLOW_EXECUTION DROP PARTITION '||aTab.PARTITION_NAME||' UPDATE INDEXES'; END IF; END LOOP; END;"
                    }
                ],
                "jobType": "Business"
            }
        ]
    }
```
